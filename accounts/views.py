from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView
from django.contrib import messages

from .decoraters import unauthenticated_user, allowed_user
from .forms import CreateUserForm, CreateUserProfile
from django.contrib.auth import authenticate, logout, login


# Create your views here.

@unauthenticated_user
def register_page(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='users')
            user.groups.add(group)
            messages.success(request, "Account was created for " + username)
            return redirect('/accounts/login')

    context = {'form': form}
    return render(request, 'accounts/register.html', context)


@unauthenticated_user
def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'Username or Password is incorrect')

    context = {}
    return render(request, 'accounts/login.html', context)


def logoutView(request):
    logout(request)
    return redirect('login')


@method_decorator(login_required, name='dispatch')
@method_decorator(allowed_user(allowed_roles=['admin']), name='dispatch')
class HomePage(TemplateView):
    template_name = 'home.html'


@login_required(login_url='login')
@allowed_user(allowed_roles=['users'])
def user_profile(request):
    form = CreateUserProfile()
    if request.method == 'POST':
        form = CreateUserProfile(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='users')
            user.groups.add(group)
            messages.success(request, "Account was created for " + username)
            return redirect('/accounts/login')

    context = {'form': form}
    return render(request, 'accounts/register.html', context)
    context = {}
    return render(request, 'accounts/user.html', context)
