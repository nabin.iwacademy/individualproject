
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import MyProfile


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class CreateUserProfile(ModelForm):
    class Meta:
        model = MyProfile
        fields = ['first_name', 'last_name', 'date_of_birth', 'address', 'status', 'gender', 'phone_no', 'description', 'pic']

