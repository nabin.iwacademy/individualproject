from django.urls import path
from django.contrib.auth import views as auth_views
from .views import login_page, register_page, logoutView, user_profile

urlpatterns = [
     path('signup/', register_page, name="signup"),
     path('login/', login_page, name="login"),
     path('logout/',logoutView, name='logout'),
     path('user/', user_profile, name ='user')


]
