from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import MyPost, PostComments, PostLike, FollowUser, MyProfile


# Register your models here.

class MyProfileAdmin(ModelAdmin):
    list_display = ['first_name', 'last_name', 'user', 'date_of_birth', 'address', 'status', 'gender', 'phone_no', 'description']
    search_fields = ['name', 'user', 'address', 'phone_no']
    list_filter = ['first_name', 'last_name', 'user', 'address', 'phone_no', ]


admin.site.register(MyProfile, MyProfileAdmin)
